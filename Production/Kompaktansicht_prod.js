var general = require('../pageObjects/PROD/Kompaktansicht');

module.exports = {
    "@tags" : ['one'],
    before: function (browser) {
        general(browser).openBrowser();
    },
    'TEST SUITE:Toplist -Production -Kompaktansicht tab' : function(browser){
        general(browser).bl1();
        general(browser).consentMessage();
        

    },

    'Kompaktansicht tab - Testing Products - Product#1 in "Handys & Smartphones" toplist' : function(browser){
        general(browser).handys();
        general(browser).bl1();
        general(browser).product1();
    },

    'Kompaktansicht tab - Testing Products - Product#2 in "Handys & Smartphones" toplist' : function(browser){
        general(browser).bl1();
        general(browser).product2();
    },

    'Kompaktansicht tab - Testing Products - Product#3 in "Handys & Smartphones" toplist' : function(browser){
        general(browser).bl1();
        general(browser).product3();
    },
    'Checking "Kompaktansicht tab" in "Handys & Smartphones" toplist' : function(browser){
        //general(browser).handys();
        general(browser).breadcrumbs();
        general(browser).title();
        general(browser).chipLogo();
        general(browser).firstParagraph();
        general(browser).weiterlesen();
        general(browser).SoTestetCHIP();
        general(browser).back();
        general(browser).consentMessage();
        general(browser).kompaktansichtText();
    },

    'Kompaktansicht tab - Check Filters > Sortieren in "Handys & Smartphones" toplist' : function(browser){
        general(browser).openSortieren();
        general(browser).preiseinschätzung();
        general(browser).preisAufsteigend();
        general(browser).preisAbsteigend();
        general(browser).goToDetailansicht1();
    },

    'Kompaktansicht tab - Check Filters > Filtern and Kategorien in "Handys & Smartphones" toplist' : function(browser){
        general(browser).preis();
        general(browser).von();
        general(browser).bis();
        general(browser).openHersteller();
        general(browser).hersteller1();
        general(browser).hersteller2();
        general(browser).yourSelection();
        general(browser).goToDetailansicht2();
        general(browser).alleFilterloschen();
        general(browser).kategorien();
        general(browser).consentMessage(); 
    },



    'Kompaktansicht tab - Section below Toplists products in "Handys & Smartphones" toplist' : function(browser){
        general(browser).bl1();
        general(browser).bottomHeadline();
        general(browser).bottomText();
        general(browser).bottomCHIPTestcenter();
        general(browser).bottomGutscheine();
        general(browser).bottomFAQs();

    },


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module