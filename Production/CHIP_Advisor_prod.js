var general = require('../pageObjects/PROD/CHIP_Advisor');

module.exports = {
    "@tags" : ['one'],
    before: function (browser) {
        general(browser).openBrowser();
    },
    'TEST SUITE:Toplist -Production -CHIP Advisor tab' : function(browser){
        general(browser).bl1();
        general(browser).consentMessage();

    },
    'Check available Advisor section in "Handys & Smartphones" toplist' : function(browser){
        general(browser).handys();
        general(browser).advisor();
    },

 




    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module