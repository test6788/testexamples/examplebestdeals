var general = require('../../pageObjects/PROD/allToplists');

module.exports = {
    "@tags" : ['one'],
    before: function (browser) {
        general(browser).openBrowser();
    },
    'TEST SUITE:Toplist1 -Production -Check all toplists' : function(browser){
        general(browser).bl1();
        general(browser).consentMessage();

    },
    'Check status code and Opening toplists' : function(browser){
        general(browser).runRandom();

    },
 

    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module