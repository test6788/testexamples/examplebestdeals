var general = require('../pageObjects/PROD/Detailansicht');

module.exports = {
    "@tags" : ['one'],
    before: function (browser) {
        general(browser).openBrowser();
    },
    'TEST SUITE:Toplist -Production -Detailansicht - NEW!' : function(browser){
        general(browser).bl1();
        general(browser).consentMessage();

    },
    'Checking "Detailansicht tab" in "Handys & Smartphones" toplist' : function(browser){
        general(browser).handys();
        general(browser).detailansichtTab();
        general(browser).produkte1to3vonX();
        general(browser).horizontaleTabelle();
        general(browser).alleFilterloschen();
        general(browser).gesamtnote();
        general(browser).preisSortieren();
        general(browser).herstellerSortieren();
        general(browser).fixedHeader();
        general(browser).singleRightArrow();
        general(browser).singleLeftArrow();
        general(browser).doubleRightArrow();
        general(browser).doubleLeftArrow();

    },

    'Check product#1 in Detailansich tab ' : function(browser){
        general(browser).product1();

    },

    'Check product#2 in Detailansich tab ' : function(browser){
        general(browser).product2();
     
    },




 

    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module