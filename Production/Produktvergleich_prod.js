var general = require('../pageObjects/PROD/Produktvergleich');

module.exports = {
    "@tags" : ['one'],
    before: function (browser) {
        general(browser).openBrowser();
    },
    'TEST SUITE:Toplist -Production -Produktvergleich -NEW!' : function(browser){
        general(browser).bl1();
        general(browser).consentMessage();

    },

    'Selecting 2 products in Kompacktansicht to compare in Produktvergleich tab' : function(browser){
        general(browser).handys();
        general(browser).selectProduct();
        general(browser).selectProduct2();
        general(browser).selectProduct3();
        general(browser).produktvergleich();
    
    },

    'Check product#1 in Produktvergleich tab ' : function(browser){
        general(browser).product1();
    },

    'Check product#2 in Produktvergleich tab ' : function(browser){
        general(browser).product2();
    },

    'Check general features in Produktvergleich tab ' : function(browser){
        general(browser).produkte1to3vonX();
        general(browser).horizontaleTabelle();
        general(browser).alleFilterloschen();
        general(browser).gesamtnote();
        general(browser).preisSortieren();
        general(browser).herstellerSortieren();
        general(browser).fixedHeader();
    },







    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module