module.exports = function (browser){
    this.openBrowser = function() {
        browser
            .url('https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html')
            .resizeWindow(1400, 3000)
        return browser
    };//end openBrowser

    this.bl1 = function(){
        browser
            .verify.ok(true, "'https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html' opened sucessfully")
            .useXpath()
            .waitForElementVisible('/html/body', 10000)
            .pause(5000)
            // Take one screenshot 
            .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/CHIPAdvisor/mainPage.png')
            .pause(3000)
    }

    this.consentMessage = function() {
        browser
        //this is to remove the message and allow to use the page
        .useCss()
        .waitForElementPresent('#sp_message_container_598129',10000)//sp_message_container_
        .execute("document.getElementById('sp_message_container_598129').removeAttribute('style')")//to remove the consent message option1 
        .pause(5000)
        .execute("document.getElementsByTagName('html')[0].classList.remove('sp-message-open')")//to make it scrollable
        .pause(5000)
        .verify.ok(true, "Consent Message")
        //end of removing the message
    }

    this.switching = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        })
    }

    this.back = function() {
        browser
        .execute(function () {
            window.history.back()
        })//to go to the previous page
        .pause(4000)
    }


    this.handys = function(){
        browser
        //Handy & Zubehör > Handys & Smartphones
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.visible("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.containsText('/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3','Handys & Smartphones')
        .click("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.ok(true,"Click on 'Handys & Smartphones'")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/CHIPAdvisor/Handys&Smartphones.png')
        .pause(2000)
    }

    this.unlockAdvisor = function() {
        browser
        //this is to remove the message and allow to use the page
        .useCss()
        .waitForElementPresent('#_os1xae3sj',10000)//sp_message_iframe_394539
        .execute("document.getElementById('_os1xae3sj').removeAttribute('overflow')")//to remove the consent message option1 
        .pause(5000)
    }

    this.advisor = function(){
        browser
        //Go to Advisor
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[6]/div[4]")
        .verify.visible("/html/body/main/div[2]/div[6]/div[4]")
        .verify.containsText('/html/body/main/div[2]/div[6]/div[4]/a/div/div[2]/div','Produktfinder')
        .verify.containsText('/html/body/main/div[2]/div[6]/div[4]/a/div/div[2]/p','Schritt-für-Schritt-Berater')
        .verify.ok(true, "Checking Produktfinder's tab")
        .waitForElementVisible('/html/body/main/div[2]/div[6]/div[4]/div')
        .verify.visible('/html/body/main/div[2]/div[6]/div[4]/div')
        .verify.ok(true, "Check 'Neu' label")
        .click('/html/body/main/div[2]/div[6]/div[4]')
        .pause(5000)
        consentMessage()
        browser
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/CHIPAdvisor/Advisor.png')
        .pause(5000)
        .execute(function() { window.scrollTo(0, 10000); }, [])
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/CHIPAdvisor/Advisor_products.png')



    }
    
    this.answer1 = function(){
        browser
        //answer1 (is selecting answer#1)
        .useXpath()
        .waitForElementVisible('/html/body/div[1]/div/div/div[3]/div[1]/div/div/label', 5000)
        .verify.visible('/html/body/div[1]/div/div/div[3]/div[1]/div/div/label')
        .click('/html/body/div[1]/div/div/div[3]/div[1]/div/div/label')
        .pause(3000)
        // Take one screenshot 
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/CHIPAdvisor/Advisorquestion1.png')
        .pause(5000)
        .verify.ok(true, "Answer#1 was selected")
        //clicking on next
        .verify.visible('/html/body/div[1]/div/div/div[4]/button')
        .click('/html/body/div[1]/div/div/div[4]/button')
        .verify.ok(true, "Click on 'next' worked")
    }

    this.name = function(){
        browser
        
    }

    this.name = function(){
        browser
        
    }

    this.name = function(){
        browser
        
    }

    this.name = function(){
        browser
        
    }

    this.name = function(){
        browser
        
    }

    this.name = function(){
        browser
        
    }

    this.name = function(){
        browser
        
    }


    return this;
}//end big module
