module.exports = function (browser){
    this.openBrowser = function() {
        browser
            .url('https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html')
            .resizeWindow(1400, 3000)
        return browser
    };//end openBrowser

    this.bl1 = function(){
        browser
            .verify.ok(true, "'https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html' opened sucessfully")
            .useXpath()
            .waitForElementVisible('/html/body', 10000)
            .pause(5000)
            // Take one screenshot 
            .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Toplists/BL1_mainPage.png')
            .pause(3000)
    }

    this.consentMessage = function() {
        browser
        //this is to remove the message and allow to use the page
        .useCss()
        .waitForElementPresent('#sp_message_container_598129',10000)//sp_message_container_
        .execute("document.getElementById('sp_message_container_598129').removeAttribute('style')")//to remove the consent message option1 
        .pause(5000)
        .execute("document.getElementsByTagName('html')[0].classList.remove('sp-message-open')")//to make it scrollable
        .pause(5000)
        .verify.ok(true, "Consent Message")
        //end of removing the message
    }

    this.switching = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        })
    }

    this.back = function() {
        browser
        .execute(function () {
            window.history.back()
        })//to go to the previous page
        .pause(4000)
    }

    this.handys = function(){
        browser
        //Handy & Zubehör > Handys & Smartphones
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3",3000)
        .verify.visible("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.containsText('/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3','Handys & Smartphones')
        .click("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.ok(true,"Click on 'Handys & Smartphones'")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Handys&Smartphones.png')
        .pause(2000)
    }

    this.breadcrumbs =  function(){
        browser
        //checking breadcrumbs
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[2]/nav",3000)
        .verify.visible("/html/body/main/div[2]/div[2]/nav")
        .assert.ok(true,"Check Breadcrumb'")
    }

    this.title = function () {
        browser
        //Checking title
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[4]/div/div[1]/h1",3000)
        .verify.visible("/html/body/main/div[2]/div[4]/div/div[1]/h1")
        .verify.containsText('/html/body/main/div[2]/div[4]/div/div[1]/h1','Die besten Handys & Smartphones')

    }

    this.chipLogo = function(){
        browser
        //Check CHIP logo
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[5]/div/div/div/div[1]/a/img",3000)
        .verify.visible("/html/body/main/div[2]/div[5]/div/div/div/div[1]/a/img")
        .assert.ok(true,"Check CHIP logo")
    }

    this.firstParagraph = function(){
        browser
        //Check first paragraph
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[1]/span",3000)
        .verify.visible("/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[1]/span")
        .assert.ok(true,"'Checking first paragraph'")
    }

    this.weiterlesen = function(){
        browser
        //Check weiterlesen
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[1]/span/a",3000)
        .verify.visible("/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[1]/span/a")
        .verify.containsText('/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[1]/span/a','... weiterlesen')
        .click('/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[1]/span/a')
        .assert.ok(true,"Check weiterlesen")
        .pause(3000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Clickonweiterlesen.png')
        .pause(3000)
    }


    this.SoTestetCHIP = function(){
        browser
        //Check So testet CHIP: Handys 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[3]/a",3000)
        .verify.visible("/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[3]/a")
        .verify.containsText('/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[3]/a','So testet CHIP: Handys')
        .click('/html/body/main/div[2]/div[5]/div/div/div/div[2]/p[3]/a')
        .assert.ok(true,"Checking 'So testet CHIP: Handys'")
    }

    this.kompaktansichtText = function(){
        browser
        //Check Kompaktansicht text 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[6]/div[1]/a/div/div[2]/div",3000)
        .verify.visible("/html/body/main/div[2]/div[6]/div[1]/a/div/div[2]/div")
        .verify.containsText('/html/body/main/div[2]/div[6]/div[1]/a/div/div[2]/div','Kompaktansicht')
        .verify.containsText('/html/body/main/div[2]/div[6]/div[1]/a/div/div[2]/p','Nur CHIP-Bewertungen')
        .assert.ok(true,"'Kompaktansicht'tab texts'")
    }

    this.openSortieren = function(){
        browser
        //Check Filters > Sortieren
        //open Rang 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[1]/div/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[1]/div/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[1]/div/span','Rang (Gesamtnote)')
        .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[1]/div/span')
        .assert.ok(true,"'Open Rang under Sortieren'")
        .pause(3000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/openRang.png')
        .pause(3000)
    }

    this.preiseinschätzung = function(){
        browser
        //Click on Preiseinschätzung 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[2]")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[2]','Preiseinschätzung')
        .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[2]')
        .assert.ok(true,"Click on 'Preiseinschätzung'")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Preiseinschätzung.png')
        .pause(5000)
    }

    this.preisAufsteigend = function(){
        browser
        //Click on  Preis (aufsteigend) 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[3]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[3]")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[3]','Preis (aufsteigend)')
        .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[3]')
        .assert.ok(true,"Click on 'Preis (aufsteigend)'")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/preis_aufsteigend.png')
        .pause(5000)
    }

    this.preisAbsteigend = function(){
        browser
        //Click on Preis (absteigend)  
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[4]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[4]")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[4]','Preis (absteigend)')
        .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[1]/div[2]/div/a[4]')
        .assert.ok(true,"Click on 'Preis (absteigend)'")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Preis_absteigend.png')
        .pause(5000)
    }

    this.goToDetailansicht1 = function(){
        browser
        //Click on Detailansicht to see if the products appear with this sortieren selection
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[6]/div[2]/a/div",3000)
        .verify.visible("/html/body/main/div[2]/div[6]/div[2]/a/div")
        .click('/html/body/main/div[2]/div[6]/div[2]/a/div')
        .assert.ok(true,"Click on Detailansicht to see if the products appear with this sortieren selection")
        consentMessage()
        browser
        .pause(8000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/checkProducts_Detailansicht1.png')
        .pause(8000)
        back()
        consentMessage()
    }

    this.goToDetailansicht2 = function(){
        browser
        //Click on Detailansicht to see if the products appear with this sortieren selection
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[6]/div[2]/a/div",3000)
        .verify.visible("/html/body/main/div[2]/div[6]/div[2]/a/div")
        .click('/html/body/main/div[2]/div[6]/div[2]/a/div')
        .assert.ok(true,"Click on Detailansicht to see if the products appear with this sortieren selection")
        consentMessage()
        browser
        .pause(8000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/checkProducts_Detailansicht2.png')
        .pause(8000)
    }

    this.alleFilterloschen = function(){
        browser
        // Alle Filter löschen
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/div/p/a')
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/div/p/a')
        .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/div/p/a','Alle Filter löschen')
        .click('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/div/p/a')
        .assert.ok(true,'Check Alle Filter löschen')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/afterClickOn_AlleFilterlöschen.png')
        .pause(8000)
        back()
        consentMessage()
    }


    this.preis = function(){
        browser
        //Check Filters > Filtern
        //Preis (Euro) 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[2]/div",5000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[2]/div")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[2]/div','Preis (Euro)')
        .assert.ok(true,"Check 'Preis (Euro)'")
        .pause(5000)
    }

    this.von = function(){
        browser
        //check von
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/label",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/label")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/label','von')
        .assert.ok(true,"Check'von'")
        .waitForElementVisible('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/input')
        .verify.visible('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/input')
        .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/input')
        .clearValue('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/input')
        .pause(3000)
        .setValue('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[1]/input', "200")
        .verify.ok(true, "Enter a 'von' Preis") 
        .pause(3000)
    }
    this.bis = function(){
        browser
        //check bis
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/label",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/label")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/label','bis')
        .assert.ok(true,"Check'bis'")
        .waitForElementVisible('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/input')
        .verify.visible('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/input')
        .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/input')
        .clearValue('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/input')
        .pause(3000)
        .setValue('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[3]/div/form/div[2]/input', "1300")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/preisVon_Bis.png')
        .pause(5000)
    }

    this.openHersteller = function(){
        browser
        //Check Filters > Hersteller 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[4]/div",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[4]/div")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[4]/div','Hersteller')
        .assert.ok(true,"Open 'Hersteller'")
    }

    this.hersteller1 = function(){
        browser
        //Select 2 options
        //first option
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[5]/div/ul/li[1]/label[1]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[5]/div/ul/li[1]/label[1]")
        .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[5]/div/ul/li[1]/label[1]')
        .assert.ok(true,"Click first option in Hersteller")
        .pause(3000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Hersteller1.png')
        .pause(3000)
    }

    this.hersteller2 = function(){
        browser
         //Second option
         .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[5]/div/ul/li[2]/label[1]",3000)
         .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[5]/div/ul/li[2]/label[1]")
         .click('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/div[5]/div/ul/li[2]/label[1]')
         .assert.ok(true,"Click second option in Hersteller")
         .pause(3000)
         .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Hersteller2.png')
         .pause(3000)
    }

    this.yourSelection = function(){
        browser
        //check selected filters on top
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[7]/div/div[1]",3000)
        .verify.visible("/html/body/main/div[2]/div[7]/div/div[1]")
        .verify.containsText('/html/body/main/div[2]/div[7]/div/div[1]','Ihre Auswahl')
        .assert.ok(true,"Check 'Ihre Auswahl'")
        //Check for preis
        .waitForElementPresent("/html/body/main/div[2]/div[7]/div/div[2]/div[1]",3000)
        .verify.visible("/html/body/main/div[2]/div[7]/div/div[2]/div[1]")
        .assert.ok(true,"Check price filter is showing in 'Ihre Auswahl'")
        //Check for Hersteller1
        .waitForElementPresent("/html/body/main/div[2]/div[7]/div/div[2]/div[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[7]/div/div[2]/div[2]")
        .assert.ok(true,"Check Hersteller1 is showing in 'Ihre Auswahl'")
        //Check for Hersteller1
        .waitForElementPresent("/html/body/main/div[2]/div[7]/div/div[2]/div[3]",3000)
        .verify.visible("/html/body/main/div[2]/div[7]/div/div[2]/div[3]")
        .assert.ok(true,"Check Hersteller1 is showing in 'Ihre Auswahl'")
        //Check Alle löschen
        .waitForElementPresent("/html/body/main/div[2]/div[7]/div/div[2]/div[4]",3000)
        .verify.visible("/html/body/main/div[2]/div[7]/div/div[2]/div[4]")
        .verify.containsText('/html/body/main/div[2]/div[7]/div/div[2]/div[4]','Alle löschen')
        // .Click('/html/body/main/div[2]/div[7]/div/div[2]/div[4]') // ****WAIT FOR A SOLUTION OF DISAPPEARING ELEMENTS****
        // .pause(10000)
        // .pause(3000)
        // .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Click_Allelöschen.png')
        // .pause(3000)
    }

    this.kategorien = function(){
        browser
        //Check Kategorien
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/h5[3]',190,44)
        .execute(function() { window.scrollTo(0, 1250); }, [])
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/h5[3]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/h5[3]")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/h5[3]','KATEGORIEN')
        .assert.ok(true,"'Check 'Kategorien' section'")
        .pause(6000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Kategorien.png')
        .pause(5000)
        //click on one of the kategories
        .verify.visible("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/ul/li[2]/ul/li[2]/div/div[2]/a")
        .click("/html/body/main/div[2]/div[8]/div[1]/div/div/div/div/ul/li[2]/ul/li[2]/div/div[2]/a")
        .assert.ok(true,"Checking 'One link of categories'")
        consentMessage()
        browser
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/OneKategory.png')
        .pause(5000)
        back()
    }
//product #1

    this.product1Number = function(){
        browser
        //First Product - Number
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[1]/div/i",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[1]/div/i")
        .assert.ok(true,"Check number of product") 
    }

    this.product1Name = function(){
        browser
        //First Product - Name
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[1]/div/h2/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[1]/div/h2/a")
        .assert.ok(true,"Check Name of product")
    }

    this.product1Image = function(){
        browser
        //First Product - image
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[1]/a/img",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[1]/a/img")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[1]/a/img', "src",function(result){
            let imageUrl = result.value
            console.log("the image url is " +imageUrl)
            const request = require('request');
            request(imageUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS IMAGE: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+imageUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Image url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[1]/a/img')
        .assert.ok(true,"'Click on image'")
        .pause(4000)
        switching()
        browser
        .pause(4000)
    }

    this.product1ZoomImage = function(){
        browser
        //First Product - image inside elements
        //Zoom the image
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[1]/span/i", 5000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[1]/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[1]/span/i')
        .assert.ok(true,"Open zoom of the image")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/zoomImageProduct1.png')
        .pause(10000)
        .assert.ok(true,"ELEMENTS INSIDE THE ZOOM IMAGE STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()
    }

    this.product1CalBox = function(){
        browser
        //First Product - calification box
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[1]/div/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[1]/div/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[1]/div/span','Gesamtnote')
        //click on Gesamtnote information
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[1]/div/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[1]/div/span/i')
        .assert.ok(true,"Click on Gesamtnote information")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Gesamtnote_info_Product1.png')
        .pause(8000)
        .assert.ok(true,"ELEMENTS INSIDE GESAMTNOTE INFORMATION STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()
    }

    this.product1Grades = function(){
        browser
        //First Product - grades
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/ul",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/ul")
        .assert.ok(true,"Check grades")
    }


    this.product1Testbericht = function(){
        browser
        //First Product - Testbericht
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[2]/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[2]/a")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[2]/a','Testbericht')
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[2]/a', "href",function(result){
            let testberichtUrl = result.value
            console.log("the Testbericht url is " +testberichtUrl)
            const request = require('request');
            request(testberichtUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Testbericht: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+testberichtUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"testbericht url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[2]/div[2]/a')
        .assert.ok(true,"Check Testbericht")
        .pause(4000)
        switching()
    }

    this.product1MehrDetails = function(){
        browser
        //First Product - Mehr details
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button",3000)
        .execute(function() { window.scrollTo(0, 500); }, [])
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button/span','Mehr Details')
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button')
        .assert.ok(true,"Open Mehr details")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Mehr_ZumTestberichtProduct1.png')
        .pause(5000)
        //Mehr details - change to Weniger details
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button/span','Weniger Details')
        .assert.ok(true,"Check Weniger details")
        //Click on Zum Testbericht inside Mehr Details
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[4]/div/div/div/div/div[3]/a", 4000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[4]/div/div/div/div/div[3]/a")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[4]/div/div/div/div/div[3]/a', "href",function(result){
            let zumTestberichtUrl = result.value
            console.log("the Zum Testbericht url is " +zumTestberichtUrl)
            const request = require('request');
            request(zumTestberichtUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Zum Testbericht: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+zumTestberichtUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Zum Testbericht url is good")
                    }
                })
            })
        .pause(2000)
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[4]/div/div/div/div/div[3]/a')
        .pause(4000)
        switching()
        browser
        .pause(6000)
        //Close Mehr Details
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[3]/button')

    }

    this.product1Preis = function(){
        browser
        //First Product - Preis
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[1]/a/p",3000)//the number
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[1]/a/p")//the number
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[1]/span','Preis')
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[1]/a', "href",function(result){
            let preisUrl = result.value
            console.log("the Preis url is " +preisUrl)
            const request = require('request');
            request(preisUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Preis: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+preisUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Preis url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[1]/a/p')
        .assert.ok(true,"Check Preis")
        .assert.ok(true,"Check Preis link. Sometimes it does not have a link")
        switching()
    }

    this.product1Preiseinchatzung = function(){
        browser
        //First Product - Preiseinchatzung
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[2]/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[2]/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[2]/span','Preiseinschätzung')
        //click on Preiseinschätzung information
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[2]/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[2]/span/i')
        .assert.ok(true,"Click on Preiseinschätzung information")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Preiseinschätzung_info_Product1.png')
        .pause(5000)
        .assert.ok(true,"ELEMENTS INSIDE Preiseinschätzung INFORMATION STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()
        browser
        //Preiseinchatzung - info below
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[2]/p",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[4]/div[2]/p")
        .assert.ok(true,"Check Preiseinschätzung information below")
    }

    this.product1ZumShop = function(){
        browser
        //First Product - Zum shop
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]','ZUM SHOP')
        //First Product - little car Zum shop
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/a/div/div[1]/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/a/div/div[1]/span")
        .assert.ok(true,"Check little car on ZUM SHOP")
        .click('html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]')
        .assert.ok(true,"Click on ZUM SHOP")
        .pause(4000)
        switching()
        browser
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/ZUMSHOP_wasSelected1.png')
        .pause(5000)
    }

    this.product1ClickoutText = function(){
        browser
        .useXpath()
        //First Product - clickout text
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/p",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/p")
    }


    this.product1Preisvergleich = function(){
        browser
        .useXpath()
        //First Product - Zum Preisvergleich 
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[2]/p/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[2]/p/a")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[2]/p/a', "href",function(result){
            let zPUrl = result.value
            console.log("the Zum Preisvergleich  url is " +zPUrl)
            const request = require('request');
            request(zPUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Zum Preisvergleich : " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+zPUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Zum Preisvergleich  url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[2]/p/a')
        .assert.ok(true,"Check Zum Preisvergleich. Sometimes there is no link in here and it shows 'Preis vom...'")
        switching()
    }

    this.product1Vergleichen = function(){
        browser
        //First Product - Product vergleichen
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[3]/a/span[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[3]/a/span[2]')
        .assert.ok(true,"Check Produkt vergleichen")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Produktvergleichen_Product1.png')
        .pause(5000)
        .refresh()//refresh the page to remove the overlay
        .pause(4000)
        consentMessage()
    }

    this.product1 = function(){
        browser
        .getText('xpath','/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]', function(result){
            let zoomElement1 = result.value
            if(zoomElement1 === 'ZUM SHOP'){
                product1Number()
                product1Name()
                product1Image()
                product1ZoomImage()
                product1CalBox()
                product1Grades()
                product1Testbericht()
                product1MehrDetails()
                product1Preis()
                product1Preiseinchatzung()
                product1ZumShop()
                product1ClickoutText()
                product1Preisvergleich()
                product1Vergleichen()
            }
            else if(zoomElement1 === "Kein Angebot"){
                browser
                .verify.ok(true,"No product available.")         
            }
        })

    }

   
    
    //PRODUCT#2
    this.product2Number = function(){
        browser
        //Second Product - Number
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[1]/div/i",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[1]/div/i")
        .assert.ok(true,"Check number of product")    
    }

    this.product2Name = function(){
        browser
        //Second Product - Name
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[1]/div/h2/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[1]/div/h2/a")
        .assert.ok(true,"Check Name of product")
    }

    this.product2Image = function(){
        browser
        //Second Product - image
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[1]/a/img",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[1]/a/img")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[1]/a/img', "src",function(result){
            let imageUrl = result.value
            console.log("the image url is " +imageUrl)
            const request = require('request');
            request(imageUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS IMAGE: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+imageUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Image url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[1]/a/img')
        .assert.ok(true,"Click on image")
        .pause(4000)
        switching()
        browser
        .pause(4000)
    }

    this.product2ZoomImage = function(){
        browser
        .useXpath()
        //Second Product - image inside elements
        //Zoom the image
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[1]/span/i",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[1]/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[1]/span/i')
        .assert.ok(true,"Open zoom of the image")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/zoomImageProduct2.png')
        .pause(5000)
        .assert.ok(true,"ELEMENTS INSIDE THE ZOOM IMAGE STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()
    }

    this.product2CalBox = function(){
        browser
        //Second Product - calification box
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div/span','Gesamtnote')
        //click on Gesamtnote information
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/div/span/i')
        .assert.ok(true,"Click on Gesamtnote information")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Gesamtnote_info_Product2.png')
        .pause(5000)
        .assert.ok(true,"ELEMENTS INSIDE GESAMTNOTE INFORMATION STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()
    }

    this.product2Grades = function(){
        browser
        //Second Product - grades
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/ul",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/ul")
        .assert.ok(true,"Check grades")
    }

    this.product2Testbericht = function(){
        browser
        //Second Product - Testbericht
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/a")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/a','Testbericht')
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/a', "href",function(result){
            let testberichtUrl = result.value
            console.log("the Testbericht url is " +testberichtUrl)
            const request = require('request');
            request(testberichtUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Testbericht: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+testberichtUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"testbericht url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/a')
        .assert.ok(true,"Check Testbericht")
        .pause(4000)
        switching()
        browser
        .pause(4000)
    }

    this.product2MehrDetails = function(){
        browser
        //Second Product - Mehr details
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button",3000)
        .execute(function() { window.scrollTo(0, 700); }, [])
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button','Mehr Details')
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button')
        .assert.ok(true,"Open Mehr details")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Mehr_ZumTestberichtProduct2.png')
        .pause(5000)
        //Mehr details - change to Weniger details
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button','Weniger Details')
        .assert.ok(true,"Check Weniger details")
        //Click on Zum Testbericht inside Mehr Details
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[4]/div/div/div/div/div[3]/a", 3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[4]/div/div/div/div/div[3]/a")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[4]/div/div/div/div/div[3]/a', "href",function(result){
            let zumTestberichtUrl = result.value
            console.log("the Zum Testbericht url is " +zumTestberichtUrl)
            const request = require('request');
            request(zumTestberichtUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Zum Testbericht: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+zumTestberichtUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Zum Testbericht url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[4]/div/div/div/div/div[3]/a')
        .pause(4000)
        switching()
        browser
        .pause(4000)
        .useXpath()
        //Close Mehr Details
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[3]/button')
        .pause(4000)
    }

    this.product2Preis = function(){
        browser
        //Second Product - Preis
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[1]/a/p",3000)//the number
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[1]/a/p")//the number
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[1]/span','Preis')
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[1]/a', "href",function(result){
            let preisUrl = result.value
            console.log("the Preis url is " +preisUrl)
            const request = require('request');
            request(preisUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Preis: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+preisUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Preis url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[1]/a/p')//the number
        .assert.ok(true,"Check Preis")
        .assert.ok(true,"Check Preis link. Sometimes it does not have a link")
        switching()
        browser
        .pause(4000)
    }

    this.product2Preiseinchatzung = function(){
        browser
        //Second Product - Preiseinchatzung
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[2]/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[2]/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[2]/span','Preiseinschätzung')
        //click on Preiseinschätzung information
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[2]/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[2]/span/i')
        .assert.ok(true,"Click on Preiseinschätzung information")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Preiseinschätzung_info_Product2.png')
        .pause(5000)
        .assert.ok(true,"ELEMENTS INSIDE Preiseinschätzung INFORMATION STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()
        browser
        //Preiseinchatzung - info below
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[2]/p",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[4]/div[2]/p")
        .assert.ok(true,"Check Preiseinschätzung information below")
    }

    this.product2ZumShop = function(){
        browser
        //Second Product - Zum shop
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]','ZUM SHOP')
        //Second Product - little car Zum shop
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/a/div/div[1]/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/a/div/div[1]/span")
        .assert.ok(true,"Check little car on ZUM SHOP")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]')
        .assert.ok(true,"Click on ZUM SHOP")
        .pause(4000)
        switching()
        browser
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/ZUMSHOP_wasSelected_product2.png')
        .pause(5000)
    }

    this.product2ClickoutText = function(){
        browser
        .useXpath()
        //Second Product - clickout text
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/p",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/p")
    }

    this.product2Preisvergleich = function(){
        browser
        //Second Product - Zum Preisvergleich 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[2]/p",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[2]/p")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[2]/p/a', "href",function(result){
            let zPUrl = result.value
            console.log("the Zum Preisvergleich  url is " +zPUrl)
            const request = require('request');
            request(zPUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Zum Preisvergleich : " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+zPUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Zum Preisvergleich  url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[2]/p')
        .assert.ok(true,"Check Zum Preisvergleich")
        .assert.ok(true,"Check Zum Preisvergleich. Sometimes there is no link in here and it shows 'Preis vom...'")
        switching()
        browser
        .pause(4000)
    }

    this.product2Vergleichen = function(){
        browser
        //Second Product - Product vergleichen
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[3]/a/span[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[3]/a/span[2]')
        .assert.ok(true,"Check Produkt vergleichen")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Produktvergleichen_Product2.png')
        .pause(5000)
        .refresh()//refresh the page to remove the overlay
        .pause(4000)
        consentMessage()

    }

    this.product2 = function(){
        browser
        .getText('xpath','/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]', function(result){
            let zoomElement2 = result.value
            if (zoomElement2 === 'ZUM SHOP'){
                product2Number()
                product2Name()
                product2Image()
                product2ZoomImage()
                product2CalBox()
                product2Grades()
                product2Testbericht()
                product2MehrDetails()
                product2Preis()
                product2Preiseinchatzung()
                product2ZumShop()
                product2ClickoutText()
                product2Preisvergleich()
                product2Vergleichen()
            }
            else if (zoomElement2 === "Kein Angebot"){
                browser
                .verify.ok(true,"No product available.")         
            }
        })

    }


    
//PRODUCT#3
    this.product3Number = function(){
        browser
        //Third Product - Number
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[1]/div/i', 30, 10)
        .execute(function() { window.scrollTo(0, 1000); }, [])
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[1]/div/i",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[1]/div/i")
        .assert.ok(true,"Check number of product")  
    }

    this.product3Name = function(){
        browser
        //Third Product - Name
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[1]/div/h2/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[1]/div/h2/a")
        .assert.ok(true,"Check Name of product")
        
    }

    this.product3Image = function(){
        browser
        //Third Product - image
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[1]/a/img",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[1]/a/img")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[1]/a/img', "src",function(result){
            let imageUrl = result.value
            console.log("the image url is " +imageUrl)
            const request = require('request');
            request(imageUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS IMAGE: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+imageUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Image url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[1]/a/img')
        .assert.ok(true,"'Click on image'")
        .pause(4000)
        switching()
        browser
        .pause(4000)
    }

    this.product3ZoomImage = function(){
        browser
        //Third Product - image inside elements
        //Zoom the image
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[1]/span/i",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[1]/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[1]/span/i')
        .assert.ok(true,"Open zoom of the image")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/zoomImageProduct3.png')
        .pause(5000)
        .assert.ok(true,"ELEMENTS INSIDE THE ZOOM IMAGE STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()

    }

    this.product3CalBox = function(){
        browser
        //Third Product - calification box
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[1]/div/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[1]/div/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[1]/div/span','Gesamtnote')
        //click on Gesamtnote information
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[1]/div/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[1]/div/span/i')
        .assert.ok(true,"Click on Gesamtnote information")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Gesamtnote_info_Product3.png')
        .pause(5000)
        .assert.ok(true,"ELEMENTS INSIDE GESAMTNOTE INFORMATION STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()

    }

    this.product3Grades = function(){
        browser
        //Third Product - grades
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/ul",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/ul")
        .assert.ok(true,"Check grades")
    }

    this.product3Testbericht = function(){
        browser
        //Third Product - Testbericht
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[2]/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[2]/a")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[2]/a','Testbericht')
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[2]/a', "href",function(result){
            let testberichtUrl = result.value
            console.log("the Testbericht url is " +testberichtUrl)
            const request = require('request');
            request(testberichtUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Testbericht: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+testberichtUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"testbericht url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[2]/div[2]/a')
        .assert.ok(true,"Check Testbericht")
        .pause(4000)
        switching()
        browser
        .pause(4000)

    }


    this.product3MehrDetails = function(){
        browser
        //Third Product - Mehr details
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button', 30, 10)
        .execute(function() { window.scrollTo(0, 1000); }, [])
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button", 5000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button','Mehr Details')
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button')
        .assert.ok(true,"Open Mehr details")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Mehr_ZumTestberichtProduct3.png')
        .pause(5000)
        //Mehr details - change to Weniger details
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button/span','Weniger Details')
        .assert.ok(true,"Check Weniger details")
        //Click on Zum Testbericht inside Mehr Details
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[4]/div/div/div/div/div[3]/a", 3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[4]/div/div/div/div/div[3]/a")
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[4]/div/div/div/div/div[3]/a', "href",function(result){
            let zumTestberichtUrl = result.value
            console.log("the Zum Testbericht url is " +zumTestberichtUrl)
            const request = require('request');
            request(zumTestberichtUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Zum Testbericht: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+zumTestberichtUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Zum Testbericht url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[4]/div/div/div/div/div[3]/a')
        .pause(4000)
        switching()
        browser
        .pause(6000)
        //Close Mehr Details
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[3]/button')
        switching()
        browser
        .pause(6000)
    }

    this.product3Preis = function(){
        browser
        //Third Product - Preis
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[1]/a/p",3000)//the number
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[1]/a/p")//the number
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[1]/span','Preis')
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[1]/a', "href",function(result){
            let preisUrl = result.value
            console.log("the Preis url is " +preisUrl)
            const request = require('request');
            request(preisUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Preis: " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+preisUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Preis url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[1]/a/p')//the number
        .assert.ok(true,"Check Preis")
        .assert.ok(true,"Check Preis link. Sometimes it does not have a link")
        switching()
        browser
        .pause(4000)

    }

    this.product3Preiseinchatzung = function(){
        browser
        //Third Product - Preiseinchatzung
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[2]/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[2]/span")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[2]/span','Preiseinschätzung')
        //click on Preiseinschätzung information
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[2]/span/i")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[2]/span/i')
        .assert.ok(true,"Click on Preiseinschätzung information")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Preiseinschätzung_info_Product3.png')
        .pause(5000)
        .assert.ok(true,"ELEMENTS INSIDE Preiseinschätzung INFORMATION STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY")
        .refresh()//refresh the page to remove the overlay
        consentMessage()
        browser
        //Preiseinchatzung - info below
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[2]/p",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[4]/div[2]/p")
        .assert.ok(true,"Check Preiseinschätzung information below")

    }

    this.product3ZumShop = function(){
        browser
        //Third Product - Zum shop
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]','ZUM SHOP')
        //Third Product - little car Zum shop
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/a/div/div[1]/span",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/a/div/div[1]/span")
        .assert.ok(true,"Check little car on ZUM SHOP")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/a/div/div[1]/span')
        .assert.ok(true,"Click on ZUM SHOP")
        .pause(4000)
        switching()
        browser
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/ZUMSHOP_wasSelected_product3.png')
        .pause(5000)
    }

    this.product3ClickoutText = function(){
        browser
        .useXpath()
        //Third Product - clickout text
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/p",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/p")

    }

    this.product3Preisvergleich = function(){
        browser
        //Third Product - Zum Preisvergleich 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[2]/p/a",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[2]/p/a")
        .verify.containsText('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[2]/p/a','Zum Preisvergleich')
        .getAttribute('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[2]/p/a', "href",function(result){
            let zPUrl = result.value
            console.log("the Zum Preisvergleich  url is " +zPUrl)
            const request = require('request');
            request(zPUrl, function (error, response, body) {
                    let responseURL = response.statusCode;
                    console.log("RESPONSE STATUS Zum Preisvergleich : " + responseURL)
                    if (responseURL === 404){
                        browser
                        .verify.equal(response.statusCode, 200)
                        .verify.ok(false,"this link does not work: "+zPUrl)
                    }
                    else {
                        browser
                        .verify.ok(true,"Zum Preisvergleich  url is good")
                    }
                })
            })
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[2]/p/a')
        .assert.ok(true,"Check Zum Preisvergleich")
        .assert.ok(true,"Check Zum Preisvergleich. Sometimes there is no link in here and it shows 'Preis vom...'")
        switching()
        browser
        .pause(5000)
    }

    this.product3Vergleichen = function(){
        browser
        //Third Product - Product vergleichen
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[3]/a/span[2]', 153,16)
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[3]/a/span[2]",3000)
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[3]/a/span[2]')
        .assert.ok(true,"Check Produkt vergleichen")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/Produktvergleichen_Product3.png')
        .pause(5000)
        .refresh()//refresh the page to remove the overlay
        .pause(4000)
        consentMessage()
    }
    this.product3 = function(){
        browser
        .getText('xpath','/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[1]/div/a/div/div[2]', function(result){
            let zoomElement3 = result.value
            if (zoomElement3 === 'ZUM SHOP'){
                product3Number()
                product3Name()
                product3Image()
                product3ZoomImage()
                product3CalBox()
                product3Grades()
                product3Testbericht()
                product3MehrDetails()
                product3Preis()
                product3Preiseinchatzung()
                product3ZumShop()
                product3ClickoutText()
                product3Preisvergleich()
                product3Vergleichen()
            }
            else if(zoomElement3 === "Kein Angebot"){
                browser
                .verify.ok(true,"No product available.")         
            }
    
        })

    }

   

   

    //bottom page

    this.bottomHeadline = function(){
        browser
        //Check headline at the bottom
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[9]/div[1]/h2', 30, 10)
        .waitForElementPresent("/html/body/main/div[2]/div[9]/div[1]/h2",5000)
        .verify.visible("/html/body/main/div[2]/div[9]/div[1]/h2")
        .assert.ok(true,"Check headline at the bottom") 
    }

    this.bottomText = function(){
        browser
        //Check text at the bottom
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[9]/div[1]/div', 30, 10)
        .waitForElementPresent("/html/body/main/div[2]/div[9]/div[1]/div",3000)
        .verify.visible("/html/body/main/div[2]/div[9]/div[1]/div")
        .assert.ok(true,"Check text at the bottom of Toplist's products") 
    }

    this.bottomCHIPTestcenter = function(){
        browser
        //Check 'So testet das CHIP testcenter...'
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[9]/div[2]/h2', 30, 10)//to the FAQ SECTION
        .waitForElementPresent("/html/body/main/div[2]/div[9]/div[1]/div/div[1]/a", 10000)
        .verify.visible("/html/body/main/div[2]/div[9]/div[1]/div/div[1]/a")
        .click('/html/body/main/div[2]/div[9]/div[1]/div/div[1]/a')
        .assert.ok(true,"Check 'So testet das CHIP testcenter...'")
        .pause(6000)
        switching()
        browser
        .pause(6000)
    }

    this.bottomGutscheine = function(){
        browser
        //Check 'Gutscheine zum thema...'
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[9]/div[2]/h2', 30, 10)//to the FAQ SECTION
        .waitForElementPresent("/html/body/main/div[2]/div[9]/div[1]/div/h3[8]",10000)
        .verify.visible("/html/body/main/div[2]/div[9]/div[1]/div/h3[8]")
        //Click on one link
        .click('/html/body/main/div[2]/div[9]/div[1]/div/div[2]/a')
        .assert.ok(true,"Check 'Gutscheine zum thema...'") 
        .pause(6000)
        switching()
        browser
        .pause(6000)
    }

    this.bottomFAQs = function(){
        browser
        //Check FAQs section
        .useXpath()
        //.moveToElement('//*[@id="acc_1-0_panel_8_trigger"]', 30, 10)//to the FAQ SECTION
        .waitForElementPresent("/html/body/main/div[2]/div[9]/div[2]/h2", 10000)
        .verify.visible("/html/body/main/div[2]/div[9]/div[2]/h2")
        //Open one of the FAQs
        .waitForElementPresent('//*[@id="acc_1-0_panel_1_trigger"]')
        .verify.visible('//*[@id="acc_1-0_panel_1_trigger"]')
        .click('//*[@id="acc_1-0_panel_1_trigger"]')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Kompaktansicht/FAQS.png')
        .pause(5000)
        .assert.ok(true,"Check FAQs section") 
    }

 



   




    return this;
}//end big module
