module.exports = function (browser){
    this.openBrowser = function() {
        browser
            .url('https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html')
            .resizeWindow(1400, 3000)
        return browser
    };//end openBrowser

    this.bl1 = function(){
        browser
            .verify.ok(true, "'https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html' opened sucessfully")
            .useXpath()
            .waitForElementVisible('/html/body', 10000)
            .pause(5000)
            // Take one screenshot 
            .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Toplists/BL1_mainPage.png')
            .pause(3000)
    }

    this.consentMessage = function() {
        browser
        //this is to remove the message and allow to use the page
        .useCss()
        .waitForElementPresent('#sp_message_container_598129',10000)//sp_message_container_
        .execute("document.getElementById('sp_message_container_598129').removeAttribute('style')")//to remove the consent message option1 
        .pause(5000)
        .execute("document.getElementsByTagName('html')[0].classList.remove('sp-message-open')")//to make it scrollable
        .pause(5000)
        .verify.ok(true, "Consent Message")
        //end of removing the message
    }

    this.back = function() {
        browser
        .execute(function () {
            window.history.back()
        })//to go to the previous page
        .pause(2000)
    }

    this.switching = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            let newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        })
    }

    this.closeTab = function(){
        browser
        .windowHandles(function(result) {
            let newWindow;
            newWindow = result.value[1];
            
            if (newWindow){
                this.switchWindow(newWindow);
                this.closeWindow()
            }
            else{
                browser
                console.log('this is tab 0, do not close')
            }  
        })
    }
    
  

    this.handynZubehoer = function(){
        browser
        //Handy & Zubehör 
        .useXpath()
        let hnZ = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/h2'
        browser
        .waitForElementPresent(hnZ,5000)
        .verify.visible(hnZ)
        .verify.containsText(hnZ,'HANDY & ZUBEHÖR')

        .elements('css selector','div.mt-lg:nth-child(3) > div:nth-child(1) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let handyUl = result.value.length
            console.log('toplist handy length: '+handyUl)

            var handyList = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < handyUl; i++) {
                forx = forx+1;
                let handyL = handyList.replace("x", forx);
                browser
                .getText('xpath',handyL, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(handyL)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(handyL, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(handyL)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })

        


        

    }

    this.notebooknZubehoer = function(){
        browser
        //Notebooks & Zubehör
        .useXpath()
        let nnZ = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[2]/h2'
        browser
        .waitForElementPresent(nnZ,5000)
        .verify.visible(nnZ)
        .verify.containsText(nnZ,'NOTEBOOKS & ZUBEHÖR')

        .elements('css selector','div.mt-lg:nth-child(3) > div:nth-child(1) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let notebookUl = result.value.length
            console.log('toplist notebook length: '+notebookUl)

            var notebookList = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < notebookUl; i++) {
                forx = forx+1;
                let notebookL = notebookList.replace("x", forx);
                browser
                .getText('xpath',notebookL, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(notebookL)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(notebookL, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(notebookL)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.fotonVideo = function(){
        browser
        //Foto & Video
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[2]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'FOTO & VIDEO')

        .elements('css selector','div.fb:nth-child(2) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[2]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.fotonZubehoer = function(){
        browser
        //Foto-Zubehör
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[2]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'FOTO-ZUBEHÖR')

        .elements('css selector','div.fb:nth-child(2) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[2]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.tablets = function(){
        browser
        //Tablets, 2-in-1 & Ebook-Reader
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[3]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'TABLETS, 2-IN-1 & EBOOK-READER')

        .elements('css selector','div.fb:nth-child(3) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[3]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.Fernseher = function(){
        browser
        //Fernseher & Home Entertainment
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[3]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'FERNSEHER & HOME ENTERTAINMENT')

        .elements('css selector','div.fb:nth-child(3) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[3]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.ssds = function(){
        browser
        //FSSDs & Festplatten
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[4]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'SSDS & FESTPLATTEN')

        .elements('css selector','div.fb:nth-child(4) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[4]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.pc = function(){
        browser
        //PC & Komponenten
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[4]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'PC & KOMPONENTEN')

        .elements('css selector','div.fb:nth-child(4) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[4]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.fitness = function(){
        browser
        //Fitness, Gesundheit, Körperpflege
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[5]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'FITNESS, GESUNDHEIT, KÖRPERPFLEGE')

        .elements('css selector','div.fb:nth-child(5) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[5]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.audio = function(){
        browser
        //Audio
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[5]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'AUDIO')

        .elements('css selector','div.fb:nth-child(5) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[5]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.prozessoren = function(){
        browser
        //Prozessoren
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[6]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'PROZESSOREN')

        .elements('css selector','div.fb:nth-child(6) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[6]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.monitore = function(){
        browser
        //Prozessoren
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[6]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'MONITORE')

        .elements('css selector','div.fb:nth-child(6) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[6]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.onlineSicherheit = function(){
        browser
        //Online Sicherheit
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[7]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'ONLINE SICHERHEIT')

        .elements('css selector','div.fb:nth-child(7) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[7]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.netzwerk = function(){
        browser
        //Netzwerk
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[7]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'NETZWERK')

        .elements('css selector','div.fb:nth-child(7) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[7]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.drucker = function(){
        browser
        //Drucker
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[8]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'DRUCKER')

        .elements('css selector','div.fb:nth-child(8) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[8]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.antivirenprogramme = function(){
        browser
        //Antivirenprogramme
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[8]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'ANTIVIRENPROGRAMME')

        .elements('css selector','div.fb:nth-child(8) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[8]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.haushalts = function(){
        browser
        //Haushalts- und Gartengeräte
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[9]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'HAUSHALTS- UND GARTENGERÄTE')

        .elements('css selector','div.fb:nth-child(9) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[9]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.speicherkarten = function(){
        browser
        //Speicherkarten & USB-Sticks
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[9]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'SPEICHERKARTEN & USB-STICKS')

        .elements('css selector','div.fb:nth-child(9) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[9]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.kuchengerate = function(){
        browser
        //Küchengeräte
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[10]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'KÜCHENGERÄTE')

        .elements('css selector','div.fb:nth-child(10) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[10]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.onlineDienste = function(){
        browser
        //Online-Dienste
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[10]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'ONLINE-DIENSTE')

        .elements('css selector','div.fb:nth-child(10) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[10]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.auto = function(){
        browser
        //Auto & E-Mobility
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[11]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'AUTO & E-MOBILITY')

        .elements('css selector','div.fb:nth-child(11) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[11]/article[1]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.werkzeug = function(){
        browser
        //Werkzeug
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[11]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'WERKZEUG')

        .elements('css selector','div.fb:nth-child(11) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[11]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.games = function(){
        browser
        //Games & Konsolen
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[12]/article[1]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'GAMES & KONSOLEN')

        .elements('css selector','div.fb:nth-child(12) > article:nth-child(1) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[12]/article[1]/ul/li/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }

    this.objektive = function(){
        browser
        //Objektive
        .useXpath()
        let nameSection = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[12]/article[2]/h2'
        browser
        .waitForElementPresent(nameSection,5000)
        .verify.visible(nameSection)
        .verify.containsText(nameSection,'OBJEKTIVE')

        .elements('css selector','div.fb:nth-child(12) > article:nth-child(2) > ul:nth-child(2) > li', function(result) {
            let toplistUl = result.value.length
            console.log('toplist length: '+toplistUl)

            var listToplist = '/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[12]/article[2]/ul/li[x]/a'
            let forx = 0 
            for (let i = 0; i < toplistUl; i++) {
                forx = forx+1;
                let toplistID = listToplist.replace("x", forx);
                browser
                .getText('xpath',toplistID, function(result) {
                    let theText = result.value
                    browser
                    .useXpath()
                    .verify.visible(toplistID)
                    .verify.ok(true,"Check Clickout Text: "+ theText)
                    .pause(2000)

                    .getAttribute(toplistID, "href",function(result){
                        let toplistUrl = result.value
                        console.log("the toplist url is " +toplistUrl)
                        const request = require('request');
                        request(toplistUrl, function (error, response, body) {
                            let responseS = response.statusCode;
                            console.log("RESPONSE STATUS TOPLIST: " + responseS)
                            if (responseS === 404){
                                browser
                                .verify.equal(response.statusCode, 200)
                                .verify.ok(false,"Toplist link does not work: "+toplistUrl)
                            }
                            else {
                                browser
                                .verify.ok(true,"Toplists: "+toplistUrl+" url is good")
                            }
                        })
                    })
                .click(toplistID)
                .pause(2000)
                consentMessage()
                browser
                back()
                consentMessage()
                browser
                .pause(2000)
                })
            }
        })
    }


    this.runRandom = function(){
        browser
        .verify.ok(true,"selecting random toplists to test")
        let toplists = ["handynZubehoer","notebooknZubehoer","fotonVideo","fotonZubehoer",
           "tablets","Fernseher","ssds","pc","fitness","audio","prozessoren","monitore",
           "onlineSicherheit","netzwerk","drucker","antivirenprogramme","haushalts",
           "speicherkarten","kuchengerate","onlineDienste","auto","werkzeug","games",
           "objektive"]


        let list = toplists.length
        let halfList = list/2
        console.log('Half list: '+halfList)

        let toplistsTest = []
        let finalToplist =[]
       
        for (let t=0; t<halfList; t++){
            let selecting = toplists[Math.floor(Math.random() * toplists.length)];
            toplistsTest.push(selecting)   
        }
        let uniqueToplists = [...new Set(toplistsTest)];
        
        console.log("selected toplists array: "+uniqueToplists)
        console.log(uniqueToplists.length + " Toplists categories to test")
        let lastToplists = uniqueToplists.join('(),')
        console.log("final toplists array: "+lastToplists)
        finalToplist.push(lastToplists)
        console.log("final array:"+finalToplist)
        eval(lastToplists)


    }





    return this;
}//end big module
