module.exports = function (browser){
    this.openBrowser = function() {
        browser
            .url('https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html')
            .resizeWindow(1400, 3000)
        return browser
    };//end openBrowser

    this.bl1 = function(){
        browser
            .verify.ok(true, "'https://www.chip.de/artikel/CHIP-Bestenlisten-Alle-Testsieger-im-ueberblick_12823667.html' opened sucessfully")
            .useXpath()
            .waitForElementVisible('/html/body', 10000)
            .pause(5000)
            // Take one screenshot 
            .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/BL1_mainPage.png')
            .pause(3000)
    }

    this.consentMessage = function() {
        browser
        //this is to remove the message and allow to use the page
        .useCss()
        .waitForElementPresent('#sp_message_container_598129',10000)//sp_message_container_
        .execute("document.getElementById('sp_message_container_598129').removeAttribute('style')")//to remove the consent message option1 
        .pause(5000)
        .execute("document.getElementsByTagName('html')[0].classList.remove('sp-message-open')")//to make it scrollable
        .pause(5000)
        .verify.ok(true, "Consent Message")
        //end of removing the message
    }

    this.switching = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        })
    }

    this.back = function() {
        browser
        .execute(function () {
            window.history.back()
        })//to go to the previous page
        .pause(4000)
    }


    this.handys = function(){
        browser
        //Handy & Zubehör > Handys & Smartphones
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.visible("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.containsText('/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3','Handys & Smartphones')
        .click("/html/body/main/div[2]/div[4]/div/div/div[2]/div[3]/div[1]/article[1]/ul/li[1]/a/h3")
        .verify.ok(true,"Click on 'Handys & Smartphones'")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/Handys&Smartphones.png')
        .pause(2000)
    }

    this.selectProduct = function(){
        browser
        //Product 1 selected
        .useXpath()
        //.moveToElement('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[3]/a/span[2]', 105, 18)
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[1]/div/div/div[2]/div[5]/div[3]/a/span[2]')
        .verify.ok(true,"Product 1 selected")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/1productSelected.png')
        .pause(5000)
    }


    this.selectProduct2 = function(){
        browser
        //products 2 selected 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[2]/div/div/div[2]/div[5]/div[3]/a/span[2]')
        .verify.ok(true,"Product 2 selected")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/2productsSelected.png')
        .pause(5000)
        //close popup to select a third product
        .useCss()
            .waitForElementPresent('html body.is-sticky.is-smartphone.bg-gray-ls.is-desktop div.Modal__Container.is-visible div#comparison-select.Modal.bg-white.shadow-2dp.copy-md.ce-xy.ml-xs.mr-xs div.container-fluid.text-center.fb.fb-trim.pb-lg',20000, 'Some message here to show while running test')
            .pause(1000)
            .execute(function(){
                document.querySelector('html body.is-sticky.is-smartphone.bg-gray-ls.is-desktop div.Modal__Container.is-visible div#comparison-select.Modal.bg-white.shadow-2dp.copy-md.ce-xy.ml-xs.mr-xs div.container-fluid.text-center.fb.fb-trim.pb-lg div.fb-col.fb-col-trim div.mt-sm.Comparison__ActionButtons.flex a.Button.Button--Continue-Selection.mr-0.mb-xxs-so').click()
            })
        .verify.ok(true,"close popup to select a third product")
        //.refresh()//to remove the overlay
        .pause(5000)
        //consentMessage()

    }

    this.selectProduct3 = function(){
        browser
        //products 3 selected 
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .verify.visible("/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[3]/a/span[2]")
        .click('/html/body/main/div[2]/div[8]/div[2]/div[3]/div/div/div[2]/div[5]/div[3]/a/span[2]')
        .verify.ok(true,"Product 3 selected")
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/3productsSelected.png')
        .pause(5000)
    }

       //****NEW COMPARINSON SELECTION MODE-- SELECTING INFINITE PRODUCTS */ NO EXTENDED FILTERS

       this.produktvergleich = function(){
           browser
            //Go to Produktvergleich
            .useXpath()
            .pause(5000)
            .verify.ok(true,"Go to Produktvergleich")
            .useCss()
            .waitForElementPresent('html body.is-sticky.is-smartphone.bg-gray-ls.is-desktop div.Modal__Container.is-visible div#comparison-select.Modal.bg-white.shadow-2dp.copy-md.ce-xy.ml-xs.mr-xs div.container-fluid.text-center.fb.fb-trim.pb-lg',20000, 'Some message here to show while running test')
            .pause(1000)
            .execute(function(){
                document.querySelector('html body.is-sticky.is-smartphone.bg-gray-ls.is-desktop div.Modal__Container.is-visible div#comparison-select.Modal.bg-white.shadow-2dp.copy-md.ce-xy.ml-xs.mr-xs div.container-fluid.text-center.fb.fb-trim.pb-lg div.fb-col.fb-col-trim div.mt-sm.Comparison__ActionButtons.flex a.Button.Button--Dialog').click()
            })
            consentMessage()
            browser
            .pause(5000)
            .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/Produktvergleich.png')
            .pause(5000)
    }

    this.produkte1to3vonX = function(){
        browser
        .refresh()
        consentMessage()
        browser
        //Check left side elements ****different xpath from prod
        //Produkte 1 - 3 von x 
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]',5000)
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]')
        .verify.ok(true,'Check Produkte 1 - 3 von x')
    }

    this.horizontaleTabelle = function(){
        browser
        // Produkte als horizontale Tabelle anzeigen 
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/a')
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/a')
        .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/a','Produkte als horizontale Tabelle anzeigen')
        .click('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/a')
        .verify.ok(true,'Check Produkte als horizontale Tabelle anzeigen')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/HorizontalView.png')
        .pause(5000)
        .verify.ok(true,'ELEMENTS INSIDE HORIZONTAL TABLE STILL PENDING TO TEST BECAUSE STILL NEED TO FIND A WAY TO INTERACT WITH MODAL OVERLAY')
        .refresh()//refresh the page to remove the overlay
        .pause(4000)
        consentMessage()
    }

    this.alleFilterloschen = function(){
        browser
        // Alle Filter löschen
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/div/p/a')
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/div/p/a')
        .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[1]/div[1]/div/p/a','Alle Filter löschen')
        .assert.ok(true,'Check Alle Filter löschen')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/afterClickOn_AlleFilterlöschen.png')
        .pause(5000)
    }

    this.gesamtnote = function(){
        browser
        //CHIP Gesamtnote section
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]/div/b')
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]/div/b')
        .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]/div/b','CHIP Gesamtnote')
        .verify.ok(true,'Check CHIP Gesamtnote')
        //CHIP Gesamtnote section- Information overlay
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]/div/span')
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]/div/span')
        .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[1]/div/span')
        .verify.ok(true,'Check CHIP Gesamtnote section- Information overlay')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/GesamtnoteInfo.png')
        .pause(5000)
        //check "Mehr Detail zum CHIP-Testcenter " inside the tooltip
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div[2]/div[2]/p[2]/a', 5000)
        .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div[2]/div[2]/p[2]/a','Mehr Details zum CHIP-Testcenter')
        .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div[2]/div[2]/p[2]/a')
        consentMessage()
        switching()
        browser
        .useXpath()
        .verify.ok(true,'click on Mehr Detail zum CHIP-Testcenter')
        .refresh()//refresh the page to remove the overlay
        .pause(4000)
        consentMessage()
    }

    

    this.preisSortieren = function(){
        browser
        //preis
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[1]/div/div', 5000)
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[1]/div/div')
        .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[1]/div/div','Preis')
        .assert.ok(true,'Checking preis')
        // overlay preis sortieren
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[1]/div/span', 5000)
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[1]/div/span')
        .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[1]/div/span')
        .assert.ok(true,'Check preis sortieren- Information overlay')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/PreisSortieren.png')
        .pause(5000)
        //click on sortieren to see different results
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[5]/div[2]/div[4]/div/div[2]/span',5000)
        .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[4]/div[5]/div[2]/div[4]/div/div[2]/span')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/Click_PreisSortieren.png')
        .pause(5000)
        .refresh()//refresh the page to remove the overlay
        .pause(4000)
        consentMessage()
    }

    this.herstellerSortieren = function(){
        browser
        //hersteller
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[1]/div/div', 5000)
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[1]/div/div')
        .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[1]/div/div','Hersteller')
        .assert.ok(true,'Checking hersteller')
        // overlay preis sortieren
        .useXpath()
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[1]/div/span', 5000)
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[1]/div/span')
        .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[1]/div/span')
        .assert.ok(true,'Check hersteller sortieren- Information overlay')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/herstellerSortieren.png')
        .pause(5000)
        //click on sortieren to see different results
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[5]/div[2]/div[4]/div/div[2]/span',5000)
        .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[5]/div[5]/div[2]/div[4]/div/div[2]/span')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/click_herstellerSortieren.png')
        .pause(5000)
        .refresh()//refresh the page to remove the overlay
        .pause(4000)
        consentMessage()
    }

    
    this.fixedHeader = function(){
        browser
        //Check fixed header
        .useXpath()
        .execute(function() { window.scrollTo(0, 10000); }, [])
        .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[2]')//first box in the left should be appear as a fixed header
        .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[2]')
        .verify.ok(true,'Check fixed header when we scroll down')
        .pause(5000)
        .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/FixedHeader.png')
        .pause(8000)
        .execute(function() { window.scrollTo(0,0); }, [])
        ////.moveToElement('/html/body/main/div[2]/div[7]/section[2]/div[1]/div[1]/div/b', 108,16)
        .pause(5000)
        
        
    }

        ////***product#1 *////


        this.productnumber = function(){
            browser
            //Check Product#1 - number
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[2]/div[1]/div')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[2]/div[1]/div')
            
            .assert.ok(true,'Check Product#1 - number')
        }
    
        this.productName = function(){
            browser
            //Check Product#1 - name
            .useXpath()
            .waitForElementPresent('//html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[3]/span[1]/b')
            .verify.visible('//html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[3]/span[1]/b')
            .assert.ok(true,'Check Product#1 - name')
        }
    
        this.productImage = function(){
            browser
            //Check Product#1 - image
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[3]/div/img')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[3]/div/img')
            .assert.ok(true,'Check Product#1 - image')
        }
    
        this.productPreis = function(){
            browser
            //Check Product#1 - Preis
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[3]/span[2]/span[2]')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[3]/span[2]/span[2]')
            .assert.ok(true,'Check Product#1 - Preis')
        }
    
        this.productZUMSHOP = function(){
            browser
            //Check Product#1 - ZUM SHOP
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[4]/div/a',6000)
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[4]/div/a')
            .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[4]/div/a','ZUM SHOP')
            .click('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[4]/div/a')
            .assert.ok(true,'Check Product#1 - ZUM SHOP')
            switching()
            browser
            .pause(6000)
        }
    
        this.productClickoutText = function(){
            browser
            //Check Product#1 - Clickout text
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[4]/div/div')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[4]/div/div')
            .assert.ok(true,'Check Product#1 - Clickout text')
     
        }
    
    
    
        this.productGrade = function(){
            browser
            //Check Product#1 - grade
            .useXpath()
            //.moveToElement('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/b', 370, 47)
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/b', 8000)
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/b')
            .assert.ok(true,'Check Product#1 - grade')
            .pause(5000)
            .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/gradeDropDownClose.png')
            .pause(5000)
            
    
        }
    
        this.productTestbericht = function(){
            browser
            //Check Product#1 - Testbericht
            .useXpath()
            //.moveToElement('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/span', 82, 22)
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/span', 6000)
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/span')
            .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/span','Testbericht')
            .getAttribute('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a', "href",function(result){
                let testberichtUrl = result.value
                console.log("the Testbericht url is " +testberichtUrl)
                const request = require('request');
                request(testberichtUrl, function (error, response, body) {
                        let responseURL = response.statusCode;
                        console.log("RESPONSE STATUS Testbericht: " + responseURL)
                        if (responseURL === 404){
                            browser
                            .verify.equal(response.statusCode, 200)
                            .verify.ok(false,"this link does not work: "+testberichtUrl)
                        }
                        else {
                            browser
                            .verify.ok(true,"testbericht url is good")
                        }
                    })
                })
            .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[2]/div/a/span')
            .pause(6000)
            .assert.ok(true,'Check Product#1 - Testbericht')
            switching()
            consentMessage()
            browser
            .pause(5000)
        }
    
        this.productPreisvergleich = function(){
            browser
            //Check Product#1 - Preisvergleich 
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[2]/div/span/a', 8000)
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[2]/div/span/a')
            .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[2]/div/span/a','Preisvergleich')
            .getAttribute('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[2]/div/span/a', "href",function(result){
                let zPUrl = result.value
                console.log("the Zum Preisvergleich  url is " +zPUrl)
                const request = require('request');
                request(zPUrl, function (error, response, body) {
                        let responseURL = response.statusCode;
                        console.log("RESPONSE STATUS Zum Preisvergleich : " + responseURL)
                        if (responseURL === 404){
                            browser
                            .verify.equal(response.statusCode, 200)
                            .verify.ok(false,"this link does not work: "+zPUrl)
                        }
                        else {
                            browser
                            .verify.ok(true,"Zum Preisvergleich  url is good")
                        }
                    })
                })
            .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[2]/div/span/a')
            .assert.ok(true,'Check Product#1 - Preisvergleich. Sometimes there is not Zum Preisvergleich')
            .pause(5000)
            switching()
            browser
            .pause(10000)
        }
    
        this.product1 = function(){
            browser
            .getText('xpath','/html/body/main/div/div[7]/div[2]/div/div[1]/div[2]/div[4]/div/a', function(result){
                let zoomElement1 = result.value
                if(zoomElement1 === 'ZUM SHOP'){
                    productnumber();
                    productName();
                    productImage();
                    productPreis();
                    productZUMSHOP();
                    productClickoutText();
                    productTestbericht();  
                    productPreisvergleich();  
                    productGrade();  
                }
                else if(zoomElement1 === "Kein Angebot"){
                    browser
                    .verify.ok(true,"No product available.")         
                }
            })
    
        }
    
    
                ////***product#2 *////
    
    
        this.product2number = function(){
            browser
            //Check product#2 - number
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[1]/div')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[1]/div')
            .assert.ok(true,'Check product#2 - number')
        }
    
        this.product2Name = function(){
            browser
            //Check product#2 - name
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[3]/span[1]/b')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[3]/span[1]/b')
            .assert.ok(true,'Check product#2 - name')
        }
    
        this.product2Image = function(){
            browser
            //Check product#2 - image
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[3]/div/img')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[3]/div/img')
            .assert.ok(true,'Check product#2 - image')
        }
    
        this.product2Preis = function(){
            browser
            //Check product#2 - Preis
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[3]/span[2]/span[2]')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[3]/span[2]/span[2]')
            .assert.ok(true,'Check product#2 - Preis')
        }
    
        this.product2ZUMSHOP = function(){
            browser
            //Check product#2 - ZUM SHOP
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[4]/div/a')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[4]/div/a')
            .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[4]/div/a','ZUM SHOP')
            .pause(3000)
            .click('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[4]/div/a')
            .assert.ok(true,'Check product#2 - ZUM SHOP')
            .pause(3000)
            switching()
            browser
            .pause(6000)
        }
    
        this.product2ClickoutText = function(){
            browser
            //Check product#2 - Clickout text
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[4]/div/div')
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[4]/div/div')
            .assert.ok(true,'Check product#2 - Clickout text')
        }
    
        this.product2Grade = function(){
            browser
            //Check Product#2 - grade  drop down
            .useXpath()
            //.moveToElement('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/b', 370, 47)
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/b', 8000)
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/b')
            .assert.ok(true,'Check Product#2 - grade')
            .pause(5000)
            .saveScreenshot('.../POM/POM_Toplist/Production/Screenshots_Production/Produktvergleich/2gradeDropDownOpen.png')
            .pause(5000)
      
        }
    
        this.product2Testbericht = function(){
            browser
            //Check product#2 - Testbericht
            .useXpath()
            //.moveToElement('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/span', 82, 22)
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/span', 6000)
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/span')
            .verify.containsText('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/span','Testbericht')
            .getAttribute('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a', "href",function(result){
                let testberichtUrl = result.value
                console.log("the Testbericht url is " +testberichtUrl)
                const request = require('request');
                request(testberichtUrl, function (error, response, body) {
                        let responseURL = response.statusCode;
                        console.log("RESPONSE STATUS Testbericht: " + responseURL)
                        if (responseURL === 404){
                            browser
                            .verify.equal(response.statusCode, 200)
                            .verify.ok(false,"this link does not work: "+testberichtUrl)
                        }
                        else {
                            browser
                            .verify.ok(true,"testbericht url is good")
                        }
                    })
                })
            .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[1]/div[3]/div/a/span')
            .assert.ok(true,'Check product#2 - Testbericht')
            .pause(6000)
            switching()
            consentMessage()
            browser
            .pause(6000)
        }
    
        this.product2Preisvergleich = function(){
            browser
            //Check product#2 - Preisvergleich 
            .useXpath()
            .waitForElementPresent('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[3]/div/span/a', 8000)
            .verify.visible('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[3]/div/span/a')
            .getAttribute('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[3]/div/span/a', "href",function(result){
                let zPUrl = result.value
                console.log("the Zum Preisvergleich  url is " +zPUrl)
                const request = require('request');
                request(zPUrl, function (error, response, body) {
                        let responseURL = response.statusCode;
                        console.log("RESPONSE STATUS Zum Preisvergleich : " + responseURL)
                        if (responseURL === 404){
                            browser
                            .verify.equal(response.statusCode, 200)
                            .verify.ok(false,"this link does not work: "+zPUrl)
                        }
                        else {
                            browser
                            .verify.ok(true,"Zum Preisvergleich  url is good")
                        }
                    })
                })
            .click('/html/body/main/div/div[7]/div[2]/div/div[2]/div[3]/div[3]/div/span/a')
            .assert.ok(true,'Check product#2 - Preisvergleich. Sometimes there is not Zum Preisvergleich')
            switching()
            browser
            .pause(6000)
        }
    
    
        this.product2 = function(){
            browser
            .getText('xpath','/html/body/main/div/div[7]/div[2]/div/div[1]/div[3]/div[4]/div/a', function(result){
                let zoomElement2 = result.value
                if(zoomElement2 === 'ZUM SHOP'){
                    product2number();
                    product2Name();
                    product2Image();
                    product2Preis();
                    product2ZUMSHOP();
                    product2ClickoutText(); 
                    product2Testbericht();  
                    product2Preisvergleich();  
                    product2Grade();  
                }
                else if(zoomElement2 === "Kein Angebot"){
                    browser
                    .verify.ok(true,"No product available.")         
                }
            })
    
        }
   
   


    return this;
}//end big module
